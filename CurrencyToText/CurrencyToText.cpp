﻿// CurrencyToText.cpp : Defines the entry point for the application.
//

///2. Write a program that converts currency to their string equivalent. 
// The program should take currency input from the user as a numeric value
// and display the string form of that value to the user.
// For example, if the user enters the value 512.34, the program should output the string 
// five hundred twelve dollars and thirty-four cents.
// This program should work up to values of 999,999.99.

#include "CurrencyToText.h"
#include <math.h>
#include <string>
using namespace std;

int getCents(float number) {
	double trunc = number - static_cast<int>(number); //3,141 > 0,141
	auto cents = static_cast<int>(roundf( trunc * 100));

	return cents;
}

int getDigitAtPlace(int value, int place) {
	auto digit =  (int)(value / pow(10, (place - 1))) % 10;
	return digit;
}

string getNumberName(int number) {
	auto greatestPower = static_cast<int>( log10(number) );
	int currentPower = greatestPower;
	string numberName;
	if (greatestPower > 0) {
		if (greatestPower >= 1) {
			auto units = getDigitAtPlace(number, 1);
			auto tens = getDigitAtPlace(number, 2);
			if (tens == 1 && units >= 1) {
				auto teenNumber = tens * 10 + units;
				numberName.append(mapOfTeens[teenNumber]);
			}else if (tens > 1 && units == 0){
				auto tensNumber = tens * 10 + units;
				numberName.append(mapOfTens[tensNumber]);
			}else if(tens > 1 && units > 0){
				numberName.append(mapOfTens[tens*10]);
				numberName.append("-");
				numberName.append(mapOfOnes[units]);
			}
		}
		if (greatestPower >= 2) {
			auto hundreds = getDigitAtPlace(number, 3);
			auto hundredsName = mapOfOnes[hundreds];
			hundredsName.append(" ");
			hundredsName.append(mapOfRestOfPowers[100]);
			hundredsName.append(" ");
			numberName.insert(0, hundredsName);
		}
		if (greatestPower >= 4) {
			auto units = getDigitAtPlace(number, 4);
			auto tens = getDigitAtPlace(number, 5);
			string thousandName = "";
			if (tens == 1 && units >= 1) {
				auto teenNumber = tens * 10 + units;
				thousandName.append(mapOfTeens[teenNumber]);
			}
			else if (tens > 1 && units == 0) {
				auto tensNumber = tens * 10 + units;
				thousandName.append(mapOfTens[tensNumber]);
			}
			else if (tens > 1 && units > 0) {
				thousandName.append(mapOfTens[tens * 10]);
				thousandName.append("-");
				thousandName.append(mapOfOnes[units]);
			}
			thousandName.append(" thousand ");
			numberName.insert(0, thousandName);
		}
		else if(greatestPower >= 3) {
			auto thousands = getDigitAtPlace(number, 4);
			auto thousandssName = mapOfOnes[thousands];
			thousandssName.append(" ");
			thousandssName.append(mapOfRestOfPowers[1000]);
			thousandssName.append(" ");
			numberName.insert(0, thousandssName);
		}
		if (greatestPower >= 5) {
			auto hundreds = getDigitAtPlace(number, 6);
			auto hundredsName = mapOfOnes[hundreds];
			hundredsName.append(" ");
			hundredsName.append(mapOfRestOfPowers[100]);
			hundredsName.append(" ");
			numberName.insert(0, hundredsName);
		}
		//while (currentPower > 1) {
		//	if (currentPower == 10000) {
		//		//auto powerName = mapOfTeens()
		//			//numberName.insert(0, getDigitAtPlace(number, ) )
		//			numberName.append("");
		//	}
		//	
		//}
	} else {
		auto units = getDigitAtPlace(number, 1);
		numberName.append(mapOfOnes[units]);
	}
	return numberName;
}

string convertToWords(double originalValue) {
	auto integerPart = static_cast<int>(originalValue);
	auto centsPart = getCents(originalValue);
	string total;
	if (integerPart > 0){
		auto integerName = getNumberName(integerPart);
		total.append(integerName);
		if (integerPart > 1) {
			total.append(" dollars");
		}
		else {
			total.append(" dollar");
		}
	}

	if (centsPart > 0 ) {
		if (integerPart > 0) {
			total.append(" and ");
		}
		auto centsName = getNumberName(centsPart);
		total.append(centsName);
		
		if (centsPart > 1) {
			total.append(" cents");
		}
		else {
			total.append(" cent");
		}
	}

	return total;
}

int main() {
	double currenyAmount = 999999.99;

	auto amountInWords = convertToWords(currenyAmount);
	cout << amountInWords << endl;
	
	return 0;
}