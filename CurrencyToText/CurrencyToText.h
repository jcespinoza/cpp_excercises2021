﻿// CurrencyToText.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#include <iostream>
#include <map>
#define MAX_NUMBER_TO_HANDLE = 1000000;

using namespace std;

// TODO: Reference additional headers your program requires here.
std::string convertToWords(double value);


std::map<int, std::string> mapOfOnes = {
	{1, "one"},
	{2, "two"},
	{3, "three"},
	{4, "four"},
	{5, "five"},
	{6, "six"},
	{7, "seven"},
	{8, "eight"},
	{9, "nine"},
};

std::map<int, std::string> mapOfTeens = {
	{10, "ten" },
	{11, "eleven"},
	{12, "twelve"},
	{13, "thirteen"},
	{14, "fourteen"},
	{15, "fifteen"},
	{16, "sixteen"},
	{17, "seventeen"},
	{18, "eighteen"},
	{19, "nineteen"},
};

std::map<int, std::string> mapOfTens = {
	{10, "ten"},
	{20, "twenty"},
	{30, "thirty"},
	{40, "fourty"},
	{50, "fifty"},
	{60, "sixty"},
	{70, "seventy"},
	{80, "eighty"},
	{90, "ninety"},
};

std::map<int, std::string> mapOfRestOfPowers = {
	{100, "hundred"},
	{1000, "thousand"},
	{1000000, "million"},
};

